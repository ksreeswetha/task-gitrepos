const request = require('request');
const fs = require('fs');

let repos = {};
let promises = [];

function getReposListOfGitUser(userName) {
    options =  {
        url: "https://api.github.com/users/" + userName + "/repos",
        method: 'GET',
        headers: {
            "User-Agent": userName
        }
    };
    promises.push(new Promise((resolve, reject) => {
            request(options, function(err, response, body) {
                if(err) {
                    reject(err);
                }
                if(response.statusCode == 200) {
                    let body1 = JSON.parse(body);
                    let repList = [];
                    for(let i=0; i<body1.length; i++) {
                        repList.push(body1[i].name);
                    }
                    repos[userName] = repList;
                } else {
                    repos[userName] = [response.body];
                }
                resolve(repos);
            });
        })
    );
}

function getReposListOfAllUsers(req, res) {
    if(!req.file) {
        res.send("No file chosen");
    }
    let path = req.file.path;
    let userNamesCSV = fs.readFileSync(path);
    let usersList = userNamesCSV.toString().split(',');
    usersList.forEach((userName) => {
        getReposListOfGitUser(userName);
    });
    return Promise.all(promises)
    .then( result => {
        res.send(repos);
    }).catch( err => {
        res.send(err);
    });  
}

module.exports = { getReposListOfAllUsers };