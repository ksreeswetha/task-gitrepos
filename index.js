const express = require('express');
const body_parser = require('body-parser');
const multer  = require('multer');
var upload = multer({ dest: 'uploads/' });

const gitAPI = require('./controllers/gitAPI');

const port = process.env.PORT || 3000;

const app = express();

// use body parser middleware
app.use(body_parser.json());
app.use(body_parser.urlencoded({extended: true}))
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.post('/', upload.single('gitFile'), async (req, res) => {
  await gitAPI.getReposListOfAllUsers(req, res);
});

app.listen(port);